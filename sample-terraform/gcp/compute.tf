resource "google_compute_instance" "main" {
    name = "test-bucket-from-workspace-${terraform.workspace}-2"
    machine_type = "f1-micro"

    boot_disk {
        initialize_params {
            image = "debian/debian-9"
        }
    }

    network_interface {
        network = "default"
        access_config {

        }
    }
}