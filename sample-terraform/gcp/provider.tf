// GCP dynamic credentils dont currently work properly with the Vault provider
provider "google" {
  project      = "my-project-id"
  access_token = data.vault_generic_secret.creds.data["token"]
}

provider "vault" {
  namespace = var.VAULT_NAMESPACE
  auth_login {
    path      = var.VAULT_LOGIN_PATH
    namespace = var.VAULT_NAMESPACE
    parameters = {
      role_id   = var.VAULT_APPROLE_ID
      secret_id = var.VAULT_APPROLE_SECRET_ID
    }
  }
}

// Doesnt work, cant read the gcp backend from a generic secret call
// GCP specific backend call doesnt currently exist inside the provider
data "vault_generic_secret" "creds" {
  path = "${var.VAULT_CREDS_BACKEND_PATH}/${var.VAULT_BACKEND_TYPE}/${var.VAULT_CREDS_ROLE_NAME}"
}

variable "VAULT_LOGIN_PATH" {}         // 
variable "VAULT_NAMESPACE" {}          // 
variable "VAULT_APPROLE_ID" {}         // 
variable "VAULT_APPROLE_SECRET_ID" {}  //
variable "VAULT_CREDS_BACKEND_PATH" {} // gcp
variable "VAULT_CREDS_ROLE_NAME" {}    // 
variable "VAULT_BACKEND_TYPE" {}       // token
