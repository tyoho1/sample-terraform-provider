provider "azure" {
  client_id     = data.vault_azure_access_credentials.creds.client_id
  client_secret = data.vault_azure_access_credentials.creds.client_secret
  features      = {}
}

provider "vault" {
  namespace = var.VAULT_NAMESPACE
  auth_login {
    path      = var.VAULT_LOGIN_PATH
    namespace = var.VAULT_NAMESPACE
    parameters = {
      role_id   = var.VAULT_APPROLE_ID
      secret_id = var.VAULT_APPROLE_SECRET_ID
    }
  }
}

data "vault_azure_access_credentials" "creds" {
  backend        = var.VAULT_CREDS_BACKEND_PATH
  role           = var.VAULT_CREDS_ROLE_NAME
  validate_creds = true
}

variable "VAULT_LOGIN_PATH" {}
variable "VAULT_NAMESPACE" {}
variable "VAULT_APPROLE_ID" {}
variable "VAULT_APPROLE_SECRET_ID" {}
variable "VAULT_CREDS_BACKEND_PATH" {}
variable "VAULT_CREDS_ROLE_NAME" {}
variable "VAULT_BACKEND_TYPE" {}
