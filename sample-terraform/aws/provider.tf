provider "aws" {
  region     = "us-east-1"
  access_key = data.vault_aws_access_credentials.creds.access_key
  secret_key = data.vault_aws_access_credentials.creds.secret_key
  token      = data.vault_aws_access_credentials.creds.security_token
}

// Use this type of block to use additional regions inside a single workspace
// Reference in resources with attribute `provider = aws.alternate_region`
# provider "aws" {
#   alias      = "alternate_region"
#   region     = "us-east-2"
#   access_key = data.vault_aws_access_credentials.creds.access_key
#   secret_key = data.vault_aws_access_credentials.creds.secret_key
#   token      = data.vault_aws_access_credentials.creds.security_token
# }

provider "vault" {
  namespace = var.VAULT_NAMESPACE
  auth_login {
    path      = var.VAULT_LOGIN_PATH
    namespace = var.VAULT_NAMESPACE
    parameters = {
      role_id   = var.VAULT_APPROLE_ID
      secret_id = var.VAULT_APPROLE_SECRET_ID
    }
  }
}

data "vault_aws_access_credentials" "creds" {
  backend = var.VAULT_CREDS_BACKEND_PATH
  role    = var.VAULT_CREDS_ROLE_NAME
  type    = var.VAULT_BACKEND_TYPE
}

variable "VAULT_LOGIN_PATH" {}
variable "VAULT_NAMESPACE" {}
variable "VAULT_APPROLE_ID" {}
variable "VAULT_APPROLE_SECRET_ID" {}
variable "VAULT_CREDS_BACKEND_PATH" {}
variable "VAULT_CREDS_ROLE_NAME" {}
variable "VAULT_BACKEND_TYPE" {}
